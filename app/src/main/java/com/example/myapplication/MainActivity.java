package com.example.myapplication;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private void handleSendText(String getText){
        TextView tv = findViewById(R.id.textView);
        tv.setText(getText);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void ButtonClick(View view){
        int LAUNCH_SECOND_ACTIVITY = 1;

        Intent getIntent = new Intent();
        getIntent.setAction(Intent.ACTION_GET_CONTENT);
        getIntent.setType("text/plain");
        startActivityForResult(getIntent,LAUNCH_SECOND_ACTIVITY);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        handleSendText(data.getStringExtra(Intent.EXTRA_TEXT)); // Handle text being sent
    }
    @Override
    protected void onStart() {
        super.onStart();
        // Bind to the service
       // bindService(new Intent(this, WebTitle.class), mConnection, Context.BIND_IMPORTANT);
        try {
            Thread.sleep((long)100);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }


    }
    @Override
    protected void onResume(){
        super.onResume();




    }
    @Override
    protected void onStop() {
        super.onStop();

    }

}